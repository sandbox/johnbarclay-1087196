<?php
// $Id: block_party_misc.fields.inc,v 1.0 2010/10/14 20:18:42 Exp $



function block_party_fieldsets_conf() {

   $fields['use_h3'] =  array(
      'enabled_option' => 'Allow block headers to be tagged as h3',
      'block_form_option' => 'Use H3 Element for title instead of default H2 header?',
      'schema' => array(
        'type' => 'int',
        'size' => 'tiny',
        'not null' => FALSE,
        'description' => 'Use H3 Element for title instead of default H2 header?',
        'default' => 0,
      )
    );
    $fields['hide_title'] =  array(
      'enabled_option' => 'Allow block headers to be tagged invisible',
      'block_form_option' => 'Make Block Title/Heading Invisible?',
      'schema' => array(
        'type' => 'int',
        'size' => 'tiny',
        'not null' => FALSE,
        'description' => 'Make Block Title/Heading Invisible?',
        'default' => 0,
      ),
    );
    $fields['field_blocks'] =  array(
      'enabled_option' => 'Allow blocks to come from a field in a node.  These will only be visible on that nodes page',
    );


   return $fields;
}
