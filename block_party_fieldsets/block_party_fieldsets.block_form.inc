<?php
    
/**
 * alter a block form by hiding fieldsets
 * 
 */
function block_party_fieldsets_block_form(&$form, &$form_state, $form_id) { 
  global $user; 
  
  $fieldsets = block_party_fieldsets();
  $current_values = variable_get('block_party_fieldset', array());
//  dpm('current_values'); dpm($current_values);
  foreach ($current_values as $fieldset_id => $fieldset)  {
    $valid_role_ids = array_intersect(array_keys($user->roles), array_keys($fieldset));
  //  dpm($valid_role_ids);
    $children = array(&$form);
    $i = 0;
    foreach ($fieldsets[$fieldset_id]['location'] as $key) {
      if (@$children[$i][$key]['#type'] == 'vertical_tabs') {
       $children[$i][$key]['#type'] = NULL;
      }
      $children[$i+1] = &$children[$i][$key];
      $i=$i +1;
    }
    $target_object = &$children[$i]; // need to work by reference here   
    foreach ($valid_role_ids as $role_id)  {  
      
      $settings = array_sum($fieldset[$role_id]);
      $target_object['#attributes']['class'][] = 'block-party';
      
      if ($settings & BLOCK_PARTY_FIELDSET_HIDE_FIELDSET) {
        $target_object['#attributes']['class'][] = 'element-invisible';
      }

      if ($settings & BLOCK_PARTY_FIELDSET_COLLAPSE_FIELDSET) {
        $target_object['#collapsed'] = TRUE; 
      }

      if ($settings & BLOCK_PARTY_FIELDSET_EXPAND_FIELDSET) {
         $target_object['#collapsed'] = FALSE; 
      }

      if ($settings & BLOCK_PARTY_FIELDSET_EMPHASIZE_FIELDSET) {
         $target_object['#attributes']['class'][]= ' element-emphasize';
      }
      
      if ($settings & BLOCK_PARTY_FIELDSET_MOVE_OUT_OF_VERTICAL_TABS) {
         // not implemented.  what to do? find parent vertical tab and move outside of it.
        // if no fieldsets left in vertical tabs, remove vertical tab.
      }
    }
  }

}



