<?php
// $Id: block_party.settings.inc,v 1.3.2.1 2011/02/08 06:01:00 johnbarclay Exp $

/**
 * @file
 * admin interface for block party module settings
 *
 */

function block_party_settings() {

  $form['#title'] = "Configure Block Party Preferences";

  $form['ignore_built_in_roles'] = array('#type' => 'fieldset', '#title' => t('Authenticated and Anonymous Roles'));
  $form['ignore_built_in_roles']['block_party_ignore_built_in_roles'] = array(
    '#type' => 'checkbox',
    '#title' => t('Ignore built in roles is user interface/visibility configuration (anonymous and authenticated users?'),
    '#options' => array(0 => 0, 1 => 1),
    '#default_value' =>  block_party_conf('ignore_built_in_roles'),
  );


 $form['visibility_module'] = array('#type' => 'fieldset', '#title' => t('Block Visibility Module'));
 $form['visibility_module']['block_party_visibility_module'] = array(
    '#type' => 'radios',
    '#options' => array('context' => 'Context Module', 'block' => 'Block Module'),
    '#title' => t('Which module is being used for block visibility?'),
    '#default_value' =>  block_party_conf('visibility_module'),
    '#description' => t('context and core block module are the only 2 supported by this module.'),
    );


  $form = system_settings_form($form);
  $form['#submit'][] = 'block_party_settings_submit';

  return $form;
}

function block_party_settings_submit($form, &$form_state) {
  if ($form_state['submitted']) {
    watchdog('block_party', 'Block Party settings updated.');
  }
  block_party_decontextualized_check();
}


