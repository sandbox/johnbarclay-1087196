<?php
// $Id: block_party.settings.inc,v 1.3.2.1 2011/02/08 06:01:00 johnbarclay Exp $

/**
 * @file
 * admin interface for block party module settings
 *
 */

function block_party_misc_field_blocks_form($form, &$form_state) {

  $field_blocks = block_party_misc_field_blocks_array();
 // dpm($field_blocks);
  $form['#title'] = "Field Blocks";
  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('Which fields do you want to behave as blocks?'),
    '#description' => t('Select fields you would like to appear as blocks and which regions they should be placed on.
      The fields will only appear as blocks on that page.  The weight will determine where that block shows
      up in the region.  Use a low number for it to be at the top (e.g. -20) or a high number for it to
      show up at the bottom (eg 20).  Trying to get it in the middle of a region\'s blocks will likely be
      difficult to maintain.'),
  );

  $fields = field_info_fields();
  $node_fields = array();
  foreach ($fields as $field_name => $field_data) {
    if (isset($field_data['bundles']['node'])) {
      foreach ($field_data['bundles']['node'] as $i => $bundle) {
        $node_fields[$bundle][$field_name] = TRUE;
      }
    }
  }

  foreach (list_themes() as $theme_key => $theme_object) {
    if (!$theme_key ||  !is_object($theme_object) || $theme_object->status == 0) {
      continue;
    }
    $region_options = array_merge( array('0' => '-- block disabled --'), system_region_list($theme_key, REGIONS_ALL));
    $collapsed = !($theme_key == variable_get('theme_default', NULL) || isset($field_blocks[$theme_key]));
    $form[$theme_key] = array('#type' => 'fieldset', '#title' => $theme_object->name, '#collapsed' => $collapsed, '#collapsible' => TRUE);

    foreach ($node_fields as $bundle => $fields) {
      $collapsed = !(isset($field_blocks[$theme_key][$bundle]) && $field_blocks[$theme_key][$bundle]);
      $form[$theme_key][$bundle] = array('#type' => 'fieldset', '#title' => $bundle, '#collapsed' => $collapsed, '#collapsible' => TRUE);
      foreach ($fields as $field_name => $boolean) {

        $default_region_value = (isset($field_blocks[$theme_key][$bundle][$field_name]['region']) && $field_blocks[$theme_key][$bundle][$field_name]['region']) ? $field_blocks[$theme_key][$bundle][$field_name]['region'] : 0;
        $default_weight_value = (isset($field_blocks[$theme_key][$bundle][$field_name]['weight']) && $field_blocks[$theme_key][$bundle][$field_name]['weight']) ? $field_blocks[$theme_key][$bundle][$field_name]['weight'] : '';
        //dpm($theme_key); dpm($bundle); dpm($field_name); dpm($default_value);
        $form[$theme_key][$bundle][$theme_key . '__'. $bundle . '__'. $field_name . '__' . 'region'] = array(
          '#title' => $field_name,
          '#type' => 'select',
          '#options' => $region_options,
          '#default_value' => $default_region_value,
          '#attributes' => array('class' => array('inline')),
        );

        $form[$theme_key][$bundle][$theme_key . '__'. $bundle . '__'. $field_name . '__' . 'weight'] = array(
          '#title' => 'block weight',
          '#type' => 'textfield',
          '#size' => 5,
          '#maxlength' => 3,
          '#default_value' => $default_weight_value,
          '#attributes' => array('class' => array('inline')),

        );

      }
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  $form['#submit'] = array('block_party_misc_field_blocks_submit');
  return $form;

}


function block_party_misc_field_blocks_submit($form, &$form_state) {
  $field_blocks = array();
  //debug($form_state);
  foreach(array_diff_key($form_state['values'], array('submit' => 'submit')) as $tag => $value) {
    $quad =  explode('__', $tag);
    if (count($quad) != 4) {
      continue;
    }
    list($theme_key, $bundle, $field_name, $attribute) = $quad;
  //  dpm($values);
    if ($value) {
      $field_blocks[$theme_key][$bundle][$field_name][$attribute] = $value;
    }
  }
 // dpm($field_blocks);
  variable_set('block_party_misc_field_blocks', $field_blocks);
  if ($form_state['submitted']) {
    watchdog('block_party', 'Block Party misc field blocks updated.');
  }
}
