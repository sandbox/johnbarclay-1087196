<?php
// $Id: block_party_misc.fields.inc,v 1.0 2010/10/14 20:18:42 Exp $



function block_party_misc_conf() {

    $fields['showonlyone'] =  array(
      'enabled_option' => 'Allow blocks to be configured to "show only one" block instance per region. When a block provider module (block, menu, etc.) for a given region is configured to "Show Only One", only one block of that type will appear in that region. This is handy when you have cascading menus or sidebars that you want to replace one another; but you don\'t want authors to have to tweak visibility through the block or contexts interface.',
     );
   $fields['use_h3'] =  array(
      'enabled_option' => 'Allow block headers to be tagged as h3.  This adds an attribute to blocks that theme templates can use to determine if h3 is suggested.',
      'block_form_option' => 'Use H3 Element for title instead of default H2 header?',
      'schema' => array(
        'type' => 'int',
        'size' => 'tiny',
        'not null' => FALSE,
        'description' => 'Use H3 Element for title instead of default H2 header?',
        'default' => 0,
      )
    );
    $fields['hide_title'] =  array(
      'enabled_option' => 'Allow block headers to be tagged invisible.  This is done with a css class "element-invisible" which allows the headers to be accessible without them being visible.',
      'block_form_option' => 'Make Block Title/Heading Invisible?',
      'schema' => array(
        'type' => 'int',
        'size' => 'tiny',
        'not null' => FALSE,
        'description' => 'Make Block Title/Heading Invisible?',
        'default' => 0,
      ),
    );
    $fields['field_blocks'] =  array(
      'enabled_option' => 'Allow "field blocks."  Field blocks are fields on a node which are configured to appear as a block in a particular region.  The field is edited on the the node form, but appears as a block.  The primary advantage to this is to avoid many single page blocks clogging the block system and coupling the location of the block with its sister node.',
    );
    //   this option is not visible in the block form as its intended for use cases where a
    //  single page block is edited as part of a node as a field, and the user does not see the block form at all
    // the submit form alter should take care of turning that field into a block and setting this option,
    //  the load form alter should take care of getting this blocks content in the node form
    //  'block_form_option' => 'Only show this block on the page it was created on?',
    $fields['classes'] = array(
      'enabled_option' => 'Allow block block classes to be added to blocks',
      'schema' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
        'description' => 'String containing the classes for the block.',
       ),
      );



   return $fields;
}
