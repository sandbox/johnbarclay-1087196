<?php
// $Id: block_party.settings.inc,v 1.3.2.1 2011/02/08 06:01:00 johnbarclay Exp $

/**
 * @file
 * admin interface for block party module settings
 *
 */
function block_party_misc_classes_form() {
  require_once('block_party_misc.fields.inc');

  $form['#title'] = "Class Options";
  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('Class Settings for Blocks'),
    '#description' => t('This functionality is limited to the classes applied in the outer block html'),
  );

  $form['block_party_misc_classes_textfield'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow Authors to enter classes in a text input.'),
    '#default_value' => variable_get('block_party_misc_classes_textfield', BLOCK_PARTY_MISC_CLASSES_TEXTFIELD),
  );



  $form['block_party_misc_classes_fieldset_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label for classes fieldset in block configure form if used.'),
    '#default_value' => "",
    '#size' => 60,
    '#maxlength' => 128,
    '#default_value' => variable_get('block_party_misc_classes_fieldset_label', BLOCK_PARTY_MISC_CLASSES_FIELDSET_LABEL),
  );

  $form['block_party_misc_classes_select_heading'] = array(
    '#type' => 'textfield',
    '#title' => t('Header above Classes select field that user will see when editing a block\'s classes'),
    '#default_value' => "",
    '#size' => 60,
    '#maxlength' => 128,
    '#default_value' => variable_get('block_party_misc_classes_select_heading', BLOCK_PARTY_MISC_CLASSES_SELECT_HEADING),
  );

  $options = block_party_misc_array_to_pipes(variable_get('block_party_misc_classes_select_options',array()));

  $form['block_party_misc_classes_select_options'] = array(
    '#type' => 'textarea',
    '#title' => t('Classes which may be selected.'),
    '#description' => t('enter one class per line.  If you would like to hide the classes from
      the author, use the format:  [class name]|[friendly name] such as highlight|Highlighted Block'),
    '#default_value' => $options,
    '#required' => TRUE,
    '#cols' => 30,
  );

  $form['block_party_misc_classes_textfield_multiple'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow Authors to select multiple classes from above list.'),
    '#default_value' => variable_get('block_party_misc_classes_textfield_multiple', BLOCK_PARTY_MISC_CLASSES_TEXTFIELD_MULTIPLE),
  );



  $form = system_settings_form($form);
  $form['#submit'] = array_merge(array('block_party_misc_classes_form_submit'),$form['#submit']);

  $form['classes_example_preamble']['#markup'] = '<h2>' . t('Preview of classes fields in the block edit form') . ':</h2>';
  $form['classes_example_preamble']['#weight'] = 900;
  require_once('block_party_misc.block_form.inc');
  $form['block_party_misc_sample_display'] = block_party_misc_classes_block_form_fields("sample-class class-b", TRUE);
  $form['block_party_misc_sample_display']['#weight'] = 1000;

  return $form;
}

function block_party_misc_classes_form_submit(&$form, &$form_state) {
  //dpm($form_state['values']); dpm($form);
  unset($form_state['values']['block_party_misc_classes_select']);
  unset($form_state['values']['block_party_misc_classes_textfield']);
  $form_state['values']['block_party_misc_classes_select_options'] = block_party_misc_pipes_to_array($form_state['values']['block_party_misc_classes_select_options']);
}

  function block_party_misc_array_to_pipes($array) {
    $result_text = "";
    foreach ($array as $key => $value) {
      $result_text .= $key . '|' . $value . "\n";
    }
    return $result_text;
  }

  function block_party_misc_pipes_to_array($txt) {
    dpm($txt);
    $result_array = array();
    $mappings = preg_split('/[\n\r]+/', $txt);
    foreach ($mappings as $line) {
      if (count($mapping = explode('|', trim($line))) == 2) {
        $result_array[trim($mapping[0])] = trim($mapping[1]);
      }
    }
    return $result_array;
  }
