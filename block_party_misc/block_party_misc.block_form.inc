<?php
// $Id: block_party_misc.module,v 1.0 2010/10/14 20:18:42 Exp $

/**
 * @files this file includes all the code for the fields in the block configuration form
 *   as well as the submit functions
 */


/**
 * adds custom checkboxes on block forms
 *
 * @param type $form
 * @param type $form_state
 * @param type $form_id
 */
function block_party_misc_form_custom_fields(&$form, &$form_state, $form_id) {

  $block_module = $form['module']['#value'];
  $block_delta = $form['delta']['#value'];
  $block = block_load($block_module, $block_delta);

  // generic checkbox options for block (use_h3, hide_title)
  $options = block_party_misc_options($block, 'options');
  if (count($options) > 0) {  // dont show fieldset if no options are available
    $form['visibility']['block_party']['#type'] = 'fieldset';
    $form['visibility']['block_party']['#weight'] = BLOCK_PARTY_MISC_FIELDSET_WEIGHT;
    $form['visibility']['block_party']['#title'] = variable_get('block_party_misc_fieldset_label', BLOCK_PARTY_MISC_FIELDSET_LABEL);
    $form['visibility']['block_party']['block_party_misc'] = array(
        '#type' =>  'checkboxes',
        '#title' => '',
        '#options' => $options,
        '#default_value' =>  block_party_misc_options($block, 'default_values'),
      );
  }
  if (block_party_misc_option_enabled('classes')) {
    $classes = property_exists($block, 'classes') ? $block->classes : '';
    $fieldset = block_party_misc_classes_block_form_fields($classes);
    if ($fieldset) {
      $form['visibility']['block-party-classes-fieldset'] = $fieldset;
    }
  }

  $form['#submit'][] = 'block_party_misc_form_customizations_submit';

}

function block_party_misc_classes_block_form_fields($classes, $disabled = FALSE) {

  $heading = variable_get('block_party_misc_classes_select_heading', BLOCK_PARTY_MISC_CLASSES_SELECT_HEADING);
  $existing_classes = explode(' ', $classes);
  $options = variable_get('block_party_misc_classes_select_options',array());
  $hand_entered = array_diff($existing_classes, array_keys($options));
  $hand_entered_text = join(' ', $hand_entered);


  if (count($options) > 0) {
    $classes_select = array();
    if ($multiple = variable_get('block_party_misc_classes_textfield_multiple', BLOCK_PARTY_MISC_CLASSES_TEXTFIELD_MULTIPLE)) {
      $classes_select['#size'] = min(5, count($options));
      $classes_select['#multiple'] = TRUE;
      $classes_select['#type'] = 'checkboxes';
    }
    else {
      $empty = array(0 => '-- select --');
      $options = array_merge($empty, $options);
      $classes_select['#type'] = 'select';
    }

    $options_selected = array_intersect($existing_classes,  array_keys($options));

    $classes_select['#title'] = t($heading);
    $classes_select['#default_value'] = $options_selected;
    $classes_select['#options'] = $options;
    $classes_select['#disabled'] = $disabled;



  }

  if (variable_get('block_party_misc_classes_textfield', BLOCK_PARTY_MISC_CLASSES_TEXTFIELD)) {
    $classes_textfield = array(
      '#type' => 'textfield',
      '#title' => t('Classes to add to block. Separate each one with a space.'),
      '#default_value' => $hand_entered_text,
      '#size' => 40,
      '#maxlength' => 127,
    );
    $classes_textfield['#disabled'] = $disabled;
  }

  if (@$classes_textfield || @$classes_select) {
    $collapsed = !($hand_entered_text || count($options_selected));
    $fieldset_label = variable_get('block_party_misc_classes_fieldset_label', BLOCK_PARTY_MISC_CLASSES_FIELDSET_LABEL);
    $fieldset = array('#type' => 'fieldset', '#weight' => BLOCK_PARTY_MISC_CLASSES_FIELDSET_WEIGHT, '#title' => $fieldset_label, '#collapsed' => $collapsed, '#collapsible' => TRUE);
    if (@$classes_select) {
      $fieldset['block_party_misc_classes_select_options'] = $classes_select;
      }
    if (@$classes_textfield) {
      $fieldset['block_party_misc_classes_textfield'] = $classes_textfield;
    }
    return $fieldset;
  }
  else {
    return array();
  }

}

function block_party_misc_form_customizations_submit($form, &$form_state) {

  $block_module = $form_state['values']['module'];
  $block_delta = $form_state['values']['delta'];
  $block = block_load($block_module, $block_delta);
  $values =  array();

  foreach(block_party_misc_options($block, 'options') as $field_name => $discard) {
    $values[$field_name] = ($form_state['values']['block_party_misc'][$field_name]) ? 1 : 0;
  }

  $text_classes = explode(' ', $form_state['values']['block_party_misc_classes_textfield']);
  $selected_classes = array_keys(array_filter( $form_state['values']['block_party_misc_classes_select']));
  $values['classes'] = join(' ',array_merge($text_classes, $selected_classes));

  if (count($values)) {
    block_party_block_update($block_module, $block_delta, $values);
  }
}

function block_party_misc_options($block, $type) {
  require_once('block_party_misc.fields.inc');
  $ret = array();
  $enabled = variable_get('block_party_misc_enabled', array());

  foreach (block_party_misc_conf() as $field_name => $field_conf) {
    if (isset($enabled[$field_name]) && isset($field_conf['block_form_option'])) {
      if ($type == 'default_values' && @$block->{$field_name}) {
        $ret[] = $field_name;
      } elseif ($type == 'options'  ) {
        $ret[$field_name] = $field_conf['block_form_option'];
      }
    }
  }
  return $ret;
}
