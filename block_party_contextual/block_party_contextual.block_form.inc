<?php
    
/**
 * alter a block form by hiding fieldsets
 * 
 * 
 */
function block_party_contextual_block_form(&$form, &$form_state, $form_id) { 

  $queries = $_GET;
 
           
  // set some block visibility features based on parameters of contextual links
  if ($module = check_plain($queries['block_party_module']) && $delta = check_plain($queries['block_party_delta'])) {
    $originating_block = block_load($module, $delta);
    $location = $queries['block_party_location'];
    if ($location == 'above') {
      $weight = $originating_block->weight - 1;
    } elseif ($location == 'below') {
      $weight = $originating_block->weight + 1;
    }
    $form['visibility']['block_party_weight']['#type'] = 'hidden';
    $form['visibility']['block_party_weight']['#default_value'] = $weight;
    $form['visibility']['block_party_origin']['#type'] = 'hidden';
    $form['visibility']['block_party_origin']['#default_value'] = $queries['block_party_origin'];
    $form['visibility']['block_party_region']['#type'] = 'hidden';
    $form['visibility']['block_party_region']['#default_value'] = $queries['block_party_region'];
  }

}



