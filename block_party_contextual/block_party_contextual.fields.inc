<?php
// $Id: block_party_misc.fields.inc,v 1.0 2010/10/14 20:18:42 Exp $



function block_party_contextual_conf($block = NULL) {
  
  $fields['add-block-above'] =  array(
      'enabled_option' => 'Show "add block above this" link in contextual block links',
      'block_form_option' => 'Should this block be exclusive? That is, always replace blocks above it until it is replaced by another block below it.',
      'contextual_link' => array(
        'name' => 'add_above',
      ),
     );
  
   $fields['add-block-below'] =  array(
      'enabled_option' => 'Show "add block below this" link in contextual block links',
      'block_form_option' => 'Use H3 Element for title instead of default H2 header?',
      'contextual_link' => array(
        'name' => 'add_below',
      ),
     );
   
  
   if ($block) {
     $fields['add-block-above']['#links'] = array(
       'title' => "add block above this",
       'href' => "admin/structure/block/add/",
       'query' => array(
          'destination' => request_path(),
          'block_party_origin' => request_path(),
          'block_party_region' => $block->region,
          'block_party_location' =>  'below',
          'block_party_module' => $block->module,
          'block_party_delta' => $block->delta,
          ),

        );
     
      $fields['add-block-below']['#links'] = array(
       'title' => "add block below this",
       'href' => "admin/structure/block/add/",
       'query' => array(
          'destination' => request_path(),
          'block_party_origin' => request_path(),
          'block_party_region' => $block->region,
          'block_party_location' => 'above',
          'block_party_module' => $block->module,
          'block_party_delta' => $block->delta,
          ),

        );
   }
   return $fields;
}

